2020/07/20 Hong
1. Done since last scrum:
- created "user stories"  and checklists in Trello;
- created local database and tables;
- implement "admin-user" module, but "delete" function hasn't done;
2. To do until next Scrum:
- finish user-delete function;
- implement "admin-location" module;
3. Need assistance / figure things out
- find out how to implement google,facebook login;

2020/07/25 Hong
1. Done since last scrum:
- implement "location", "category", "car", "reservation", "customer" twigs adn most part of functions;
2. To do until next Scrum:
- implement part of "user make booking" module;
3. Need assistance / figure things out
- find out how to sort data in tables in front-end.

2020/07/26 Chunjie
1. Done since last scrum:
- finish register page;
- implement google login
2. To do until next Scrum:
- remember password in login page;
- forget password and reset;
- facebook login
3. Need assistance / figure things out
- find out how use ajax to remember password;
- find out how send email link to user;

2020/07/28 Chunjie
1. Done since last scrum:
- finish reset password
2. To do until next Scrum:
- finish remember password in login page;
- finish facebook login;
3. Need assistance / figure things out
- find out how use ajax to remember password;

2020/07/30 Chunjie
1. Done since last scrum:
- finish google and facebook login and write user info in database
- finish remember password in login page;
2. To do until next Scrum:
- finish remember password in login page;
- finish use phone to reset password
3. Need assistance / figure things out
- find out how use ajax to remember password;
- find out how use ajax to remember password;

2020/07/31 Hong
1. Done since last scrum:
- implement booking step1 - step4 twig templete;
- implement googel recaptcha and payPal;
- implement chartjs;
2. To do until next Scrum:
- add validations in UI;
3. Need assistance / figure things out
- find out how to add dialog in twig.

2020/07/31 Chunjie
1. Done since last scrum:
- finish remember password
- finish login page
2. To do until next Scrum:
- add recaptcha in register page;
- add password encryption
3. Need assistance / figure things out
after check recaptcha, how redirect continue to register.

2020/08/3 Hong
1. Done since last scrum:
- add user input validation in "add-user" twig templete;
2. To do until next Scrum:
- add user input validation to all twigs;
3. Need assistance / figure things out

2020/08/03 Chunjie
1. Done since last scrum:
- finish add recaptcha
2. To do until next Scrum:
- add password encryption
- add multi language
3. Need assistance / figure things out

2020/08/04 Chunjie
1. Done since last scrum:
- finish password encryption
2. To do until next Scrum:
- add multi-language(maybe could not finish)
- add redirection in login page to index
3. Need assistance / figure things out

2020/08/4 Hong
1. Done since last scrum:
- change project templete structure, move all twigs under admin to folder admin,
  combine "add" and "update" twigs templete;
- change "user list" pagination to using ajax;
2. To do until next Scrum:
- change all pagination to using ajax for other twig templete;
- add user input validation to all twigs;
3. Need assistance / figure things out

2020/08/05 Chunjie
1. Done since last scrum:
- finish redirction
2. To do until next Scrum:
- add multi-language(maybe could not finish)
- when user enter postcode, the map will display rental place nearby
3. Need assistance / figure things out
gettext tans tag could not use in twig. it said unexpected tag.

2020/08/05 Hong
1. Done since last scrum:
- add paginations using ajax for all twig templete;
- modify add, update, delete fonctions under admin;
2. To do until next Scrum:
- use respect/validation on server-side;
3. Need assistance / figure things out

2020/08/06 Chunjie
1. Done since last scrum:
change codes(pull codes form bitbucket are codes two days before, I have to change)
2. To do until next Scrum:
- when user enter postcode, the map will display rental place nearby
- after upload to websit, google and facebook could not work.
3. Need assistance / figure things out
gettext tans tag could not use in twig. it said unexpected tag.

2020/08/06 Hong
1. Done since last scrum:
- modify payment implement;
- add middleware to verify only admin can access urls start with /adminl;
2. To do until next Scrum:
- use respect/validation on server-side;
3. Need assistance / figure things out

2020/08/07 Chunjie
1. Done since last scrum:
fix problem about facebook and google.
2. To do until next Scrum:
- when user enter postcode, the map will display rental place nearby
- indexpage
3. Need assistance / figure things out

2020/08/07 Chunjie
1. Done since last scrum:
add index page, add contact page
2. To do until next Scrum:
- when user enter postcode, the map will display rental place nearby
- add aboutus page
- add promotion page
3. Need assistance / figure things out

2020/08/07 Hong
1. Done since last scrum:
- modify booking steps twigs templete;
2. To do until next Scrum:
- use respect/validation on server-side;
3. Need assistance / figure things out

2020/08/10 Hong
1. Done since last scrum:
- add booking step1 date/time validation;
2. To do until next Scrum:
- use respect/validation on server-side;
3. Need assistance / figure things out

2020/08/10 Chunjie
1. Done since last scrum:
add about us page, change codes for interface of user or admin
2. To do until next Scrum:
- must done implement google map
- add promotion page
3. Need assistance / figure things out

2020/08/12 Hong
1. Done since last scrum:
- add some validation;
2. To do until next Scrum:
- complete payment method;
3. Need assistance / figure things out

2020/08/12 Chunjie
1. Done since last scrum:
add questions page, check login and register, add contactus page
2. To do until next Scrum:
- finish contactus page
3. Need assistance / figure things out

2020/08/13 Chunjie
1. Done since last scrum:
add google map api
2. To do until next Scrum:
- search by postal code of address map could display nearby rental place
3. Need assistance / figure things out
how search this in one input

2020/08/14 Chunjie
1. Done since last scrum:
finish search by postalcode and address to find nearyby rental place, finish client send email for questions
2. To do until next Scrum:
- test all pages
3. Need assistance / figure things out
how search this in one input

2020/08/15 Hong
1. Done since last scrum:
- modify database operations of delete when has foreign key constain;
2. To do until next Scrum:
- modify some css style and testing;
3. Need assistance / figure things out

2020/08/14 Chunjie
1. Done since last scrum:
test all codes
2. To do until next Scrum:
- write ppx
3. Need assistance / figure things out
how search this in one input

2020/08/16 Hong
1. Done since last scrum:
- combine UI with partner;
- prepare presentation and write documents.
2. To do until next Scrum:
- do presentation
3. Need assistance / figure things out