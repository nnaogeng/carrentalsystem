<?php
require_once '_setup.php';
use Respect\Validation\Validator as Validator;

$app->get('/admin/customer/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM customers");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/customer_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/customer/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT c.*, u.email FROM customers AS c, users AS u WHERE u.id = c.userId LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/customer_singlepage.html.twig', ['customerList' => $recordList]);
});

$app->get('/admin/customer/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {    
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM customers WHERE id = %d LIMIT 1", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    return $this->view->render($response, 'admin/customer_add_update.html.twig', ['customer' => $selectedRecord, 'operation' => $operation]);
});

$app->post('/admin/customer/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {            
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $firstName = $request->getParam('firstName');
        $lastName = $request->getParam('lastName');
        //$email = $request->getParam('email');
        $phoneNo = $request->getParam('phoneNo');
        $street = $request->getParam('street');
        $city = $request->getParam('city');
        $province = $request->getParam('province');    
        $postalCode = $request->getParam('postalCode');
        $userId = $request->getParam('userId');

        $customer['firstName'] = $firstName;
        $customer['lastName'] = $lastName;
        //$customer['email'] = $email;
        $customer['phoneNo'] = $phoneNo;
        $customer['street'] = $street;
        $customer['city'] = $city;
        $customer['province'] = $province;
        $customer['postalCode'] = $postalCode;
        $customer['userId'] = $userId;

        $errorList = array();
        if (!(validator::stringType()->length(1, 50)->validate($customer['firstName']))) {
            $errorList[] = "First name must be 1-50 characters long, ";
        }
        if (!(validator::stringType()->length(1, 50)->validate($customer['lastName']))) {
            $errorList[] = "Last name must be 1-50 characters long, ";
        }
        if (!(validator::numericVal()->length(10, 15)->validate($customer['phoneNo']))) {
            $errorList[] = "Phone name must be 10-15 digital numbers long, ";
        }
        if (!(validator::length(0, 100)->validate($customer['street']))) {
            $errorList[] = "Street name must be 0-100 characters long, ";
        }
        if (!(validator::length(0, 50)->validate($customer['city']))) {
            $errorList[] = "City name must be 0-50 characters long, ";
        }
        if (!(validator::length(0, 30)->validate($customer['province']))) {
            $errorList[] = "Province name must be 0-30 characters long, ";
        }
        if (!(validator::length(0, 10)->validate($customer['postalCode']))) {
            $errorList[] = "Postal code must be 0-10 characters long, ";
        }
        $user = DB::queryFirstRow("SELECT id FROM users WHERE id = %d", $userId);
        if($user) {
            $existCustomer = DB::queryFirstRow("SELECT id, firstName, lastName FROM customers WHERE userId = %d", $userId);
            if ($existCustomer) {
                if (!($operation == 'update' && $existCustomer['id'] == $args['id'])) {                    
                    $errorList[] = "This user Id already associated with customer " . $existCustomer['firstName'] . " " . $existCustomer['lastName'];
                }
            }
        } 
        else {
            $errorList[] = "This user ID doesn't exist.";
        }

        if ($errorList) {
            return $this->view->render($response, 'admin/customer_add_update.html.twig',
                    [ 'errorList' => $errorList, 'customer' => $customer, 'operation' => $operation  ]);
        } else {
            if ($operation == 'add') {
                DB::insert("customers", $customer);                                    
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'customer' ]);
            } else {
                DB::update('customers', $customer, "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => 'customer' ]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/customer/list");
    }
});

// STATE 1: first display
$app->get('/admin/customer/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM customers WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/customer_delete.html.twig', ['customer' => $selectedRecord] );
});

$app->post('/admin/customer/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $customer = DB::queryFirstRow("SELECT * FROM reservations WHERE customerId = %d ", $args['id']);
    if ($customer) {
        $message = "There are reservations under this customer. ";
        return $this->view->render($response, 'admin/delete_fail.html.twig', ['object' => "customer", 'message' => $message ]);
    }
    DB::delete('customers', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig' , ['operation' => "delete", 'object' => "customer"]);
});
