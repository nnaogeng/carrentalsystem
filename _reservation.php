<?php
require_once '_setup.php';
use Respect\Validation\Validator as Validator;

$app->get('/admin/reservation/list[/{pageNo:[0-9]+}]', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $totalRecords = DB::queryFirstField("SELECT COUNT(*) AS COUNT FROM reservations");
    $totalPages = ceil($totalRecords / ROWS_PER_PAGE);
    return $this->view->render($response, '/admin/reservation_list.html.twig', [
            'maxPages' => $totalPages,
            'pageNo' => $pageNo,
        ]);
});

$app->get('/admin/reservation/list/singlepage/{pageNo:[0-9]+}', function ($request, $response, $args) {
    $pageNo = $args['pageNo'] ?? 1;
    $recordList = DB::query("SELECT * FROM reservations LIMIT %d OFFSET %d", ROWS_PER_PAGE, ($pageNo - 1) * ROWS_PER_PAGE);
    foreach($recordList as &$record) {
        $record['pickupLocationName'] = DB::queryFirstRow("SELECT locationName FROM locations WHERE id = %d", $record['pickupLocationId'])['locationName'];
        $record['returnLocationName'] = DB::queryFirstRow("SELECT locationName FROM locations WHERE id = %d", $record['returnLocationId'])['locationName'];

    }
    //print_r($recordList);
    return $this->view->render($response, '/admin/reservation_singlepage.html.twig', ['reservationList' => $recordList]);
});

$app->get('/admin/reservation/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    // either op is add and id is not given OR op is edit and id must be given
    $operation = $args['operation'];
    if (isset($args['id'])) {
        $id = $args['id'];
    }
    if ( ($operation == 'add' && !empty($id)) || ($operation == 'update' && empty($id)) ) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    if ($operation == 'update') {
        $selectedRecord = DB::queryFirstRow("SELECT * FROM reservations WHERE id = %d", $id);
        if (!$selectedRecord) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }
    } else {
        $selectedRecord = [];
    }
    $locationList = DB::query("SELECT * FROM locations");
    $categoryList = DB::query("SELECT * FROM categories");
    return $this->view->render($response, 'admin/reservation_add_update.html.twig', ['reservation' => $selectedRecord, 'operation' => $operation, 'locationList' => $locationList, 'categoryList' => $categoryList]);
});

$app->post('/admin/reservation/{operation:add|update}[/{id:[0-9]+}]', function ($request, $response, $args) {
    if (isset($_POST['submit'])) {
        $operation = $args['operation'];

        // either op is add and id is not given OR op is edit and id must be given
        if ( ($operation == 'add' && !empty($args['id'])) || ($operation == 'update' && empty($args['id'])) ) {
            $response = $response->withStatus(404);
            return $this->view->render($response, 'admin/not_found.html.twig');
        }

        $customerId = $request->getParam('customerId');
        $categoryId = $request->getParam('categoryId');
        $startDateTime = $request->getParam('startDateTime');
        $returnDateTime = $request->getParam('returnDateTime');        
        $pickupLocationId = $request->getParam('pickupLocationName');
        $returnLocationId = $request->getParam('returnLocationName');    
        $costPerDay = $request->getParam('costPerDay');        

        $reservation['customerId'] = $customerId;
        $reservation['categoryId'] = $categoryId;
        $reservation['startDateTime'] = $startDateTime;
        $reservation['returnDateTime'] = $returnDateTime;        
        $reservation['pickupLocationId'] = $pickupLocationId;
        $reservation['returnLocationId'] = $returnLocationId;
        $reservation['costPerDay'] = $costPerDay;

        //$pickupLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $pickupLocationName);
        //$returnLocation = DB::queryFirstRow("SELECT id FROM locations WHERE locationName = %s", $returnLocationName);

        $errorList = array();
        
        if (!(validator::intVal()->length(1, 5)->validate($reservation['customerId']))) {
            $errorList[] = "Customer Id must be 1-5 digital numbers long.";
        }        
        if (!(validator::dateTime('Y-m-d H:i:s')->validate($reservation['startDateTime']))) {
            $errorList[] = "Start date/time must 'Y-m-d H:i:s' format.";
        }
        if (!(validator::dateTime('Y-m-d H:i:s')->validate($reservation['returnDateTime']))) {
            $errorList[] = "Return date/time must 'Y-m-d H:i:s' format.";
        }
        if ($reservation['startDateTime'] >= $reservation['returnDateTime']) {
            $errorList[] = "Return date/time must later than pick up date/time.";
        }
        if ($reservation['startDateTime'] <= date("Y-m-d H:i:s")) {
            $errorList[] = "Pickup date/time must later than current date/time.";
        }
        
        if ($errorList) {
            return $this->view->render($response, 'admin/reservation_add_update.html.twig',
                    [ 'errorList' => $errorList, 'reservation' => $reservation, 'locationList' => $_SESSION['locationList'], 'categoryList' => $_SESSION['categoryList'], 'operation' => $operation]);
        } else {
            if ($operation == 'add') {     
                //DB::insert("reservations", ['customerId' => $customerId, 'startDateTime' => $startDateTime, 'returnDateTime' => $returnDateTime, 'pickupLocationId' => $pickupLocationId, 'returnLocationId' => $returnLocationId, 'costPerDay' => $costPerDay]);                                
                DB::insert("reservations", $reservation);                                                
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "reservation" ]);
            } else {
                DB::update('reservations', ['customerId' => $customerId, 'categoryId' => $categoryId, 'startDateTime' => $startDateTime, 'returnDateTime' => $returnDateTime, 'pickupLocationId' => $pickupLocationId, 'returnLocationId' => $returnLocationId, 'costPerDay' => $costPerDay], "id = %d", $args['id']);
                return $this->view->render($response, 'admin/operation_success.html.twig', ['operation' => $operation, 'object' => "reservation"  ]);
            }
        }
    }
    if (isset($_POST['cancel'])) {        
        return $response->withRedirect("/admin/reservation/list");
    }
});

// STATE 1: first display
$app->get('/admin/reservation/delete/{id:[0-9]+}', function ($request, $response, $args) {
    $selectedRecord = DB::queryFirstRow("SELECT * FROM reservations WHERE id = %d", $args['id']);
    if (!$selectedRecord) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }
    return $this->view->render($response, 'admin/reservation_delete.html.twig', ['reservation' => $selectedRecord] );
});

$app->post('/admin/reservation/delete/{id:[0-9]+}', function ($request, $response, $args) {
    DB::delete('reservations', "id = %d", $args['id']);
    return $this->view->render($response, 'admin/operation_success.html.twig' , ['operation' => "delete", 'object' => "reservation" ]);
});


