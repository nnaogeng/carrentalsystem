<?php
    public function userLogin() {
        if(IS_AJAX && !$this->member) {
            $tel = $this->_post('tel', 'trim');
            $password = $this->_post('password', 'trim,md5');
            $member = M('Member')->where(array('tel' => $tel))->find();
            
            if ($member && $member['password'] === $password) {
                
                //检测是否存在微信用户需要绑定
                if ($member['wxuser_id'] == 0 && $this->wxuser) {
                    M('Member')->where(array('id' => $member['id']))->save(array('wxuser_id' => $this->wxuser_id));
                }
                
                $href = session(LASTREQUEST);
                session(MEMBER, $member['id']);
                session(LASTREQUEST, null);
                $this->ajaxReturn(array('result' => true, 'href' => $href ? $href : U('Member/index')));
            } else {
                if (empty($member)) {
                    $this->ajaxReturn(array('result' => false, 'error' => '手机号尚未注册.'));
                } else {
                    $this->ajaxReturn(array('result' => false, 'error' => '密码不正确.'));
                }
                
            }
        } else {
            $this->ajaxReturn(array('result' => false, 'error' => '非法请求.'));
        }
    }

    // 用户退出
    public function userLogout() {
        session(WXUSER, null);
        session(MEMBER, null);
        $this->success('退出成功',U('Store/Member/index'));
    }
    
    // 用户注册
    public function userRegister() {
        $tel = $this->_post('tel', 'trim');
        $password = $this->_post('password', 'trim,md5');
        $smscode = $this->_post('smscode', 'trim');
        $session_smscode = session($this->smscode);
        $user_exit = M('Member')->where(array('tel' => $tel))->find();
        if (!preg_match("/1[3458]{1}\d{9}$/", $tel) && $user_exit) {
            $this->ajaxReturn(array('result' => false, 'error' => '手机号不合法'));
        }

        $memberModel = M('Member');
        // 检测是否已注册
        $member = $memberModel-> where(array('tel' =>$tel,'status'=>1))->find();
        if (!empty($member)) {
            $this->ajaxReturn(array('result' => false, 'error' => '已是注册用户'));
        }
        
        if (time() > $session_smscode['time']  || $smscode != $session_smscode['code']) {
            $this->ajaxReturn(array('result' => false, 'error' => '验证码不正确'));  //--调试，先把验证功能关闭
        }

        $data = array('tel' => $tel, 'password' => $password, 'wxuser_id' => intval($this->wxuser_id), 'addtime' => time());
        $insert_id = $memberModel->add($data);
        
        if ($insert_id) {
            $href = session(LASTREQUEST);
            session(MEMBER, $insert_id);   //*****只是一个id值
            $this->ajaxReturn(array('result' => true, 'href' => $href ? $href : U('Member/index')));
        } else {
            $this->ajaxReturn(array('result' => false, 'error' => '操作失败', 'msg' => M('Member')->getError()));
        }
    }
    //用户更改密码
    public function changePwd(){
        $tel = $this->_post('tel','trim');
        $password = $this ->_post('password','trim');
        $smscode = $this ->_post('smscode','trim');
        $session_smscode = session($this ->smscode);
        if (time() > $session_smscode['time']  || $smscode != $session_smscode['code']) {
            $this->ajaxReturn(array('result' => false, 'error' => '验证码不正确'));  //--调试成功
        }

        $data = array('password' => md5($password), 'addtime' => time());
        $memberModel = M('Member');
        // 检测是否已注册
        $member = $memberModel-> where(array('tel' =>$tel,'status'=>1))->find();
        if (empty($member)) {
            $this->ajaxReturn(array('result' => false, 'error' => '号码尚未注册'));
        }
        
        if ($memberModel->where(array('tel'=> $tel))->save($data)) {
            $href = session(LASTREQUEST);
            session(MEMBER, $member['id']); 
            $this->ajaxReturn(array('result' => true, 'href' => $href ? $href : U('Member/index')));
        } else {
            $this->ajaxReturn(array('result' => false, 'error' => '操作失败', 'msg' => M('Member')->getError()));
        }
    }

    // ajax检测号码是否注册
    public function checkTel() {
        $tel = $this->_post('tel', 'trim');
        if (IS_AJAX && preg_match("/1[3458]{1}\d{9}$/",$tel)) {
            $memberModel = M('Member');
            $member = $memberModel->where(array('tel'=>$tel,'status'=>1))->find();
            if (!empty($member)) {
                $this->ajaxReturn(array('status' => 1, 'info' => '已注册')); 
            } else {
                $this->ajaxReturn(array('status' => 2, 'info' => '未注册')); 
            }
        } else {
            $this->ajaxReturn(array('status' => 3, 'info' => '错误的请求')); 
        }
    }

    //发送注册验证码
    public function sendSmscode() {
        session($this->smstime, null);
        $smstime = session($this->smstime);
        $tel = $this->_post('tel', 'trim');
        
        if (IS_AJAX && (!$smstime || time() > $smstime) && preg_match("/1[3458]{1}\d{9}$/",$tel)) {
            $smscode = rand(1000, 9999);  
            //发送【阿里大鱼】的验证码
            require LIB_PATH . 'ORG/Taobao-sdk-php/TopSdk.php';
            $c = new TopClient;
            $c->appkey = '23307560'; // 原23294081
            $c->secretKey = '21ef24dd4c51e20693c5db0983c433e7'; // 原0402169f466d8fed780e7f07edd25177
            $req = new AlibabaAliqinFcSmsNumSendRequest;
            $req->setSmsType("normal");
            $req->setSmsFreeSignName("注册验证");
            $req->setSmsParam('{"code":"'. $smscode .'","product":"【多多助店宝】"}');
            $req->setRecNum("{$tel}");
            $req->setSmsTemplateCode("SMS_5056863");
            $resp = $c->execute($req);
        
            if(!$resp->code) {
                //设置发送限制时间
                session($this->smstime, time() + 50);
                //设置验证码5分钟内有效
                session($this->smscode, array('code' => $smscode, 'time' => time() + 600));
            } else {
                //发送失败写入日志文件
                $log = date('Y-m-d H:i:s') . " 发送失败  sub_code：{$resp->sub_code}  sub_msg：{$resp->sub_msg}" . PHP_EOL;
                file_put_contents(RUNTIME_PATH . 'Log/smscode.log', $log, FILE_APPEND);
            }
            
            $this->ajaxReturn(array('result' => !$resp->code));
        } else {
            $this->ajaxReturn(array('result' => false, 'error' => '错误的请求'));
        }
    }

    //发送修改密码验证码
    public function sendSmsexcode(){
        session($this->smstime, null);
        $smstime = session($this->smstime);
        $tel = $this->_post('tel', 'trim');
        if (IS_AJAX && (!$smstime || time() > $smstime) && preg_match("/1[3458]{1}\d{9}$/",$tel)) {
            $smscode = rand(1000, 9999);  
            //发送【阿里大鱼】的验证码
            require LIB_PATH . 'ORG/Taobao-sdk-php/TopSdk.php';
            $c = new TopClient;
            $c->appkey = '23307560'; // 原23294081
            $c->secretKey = '21ef24dd4c51e20693c5db0983c433e7'; // 原0402169f466d8fed780e7f07edd25177
            $req = new AlibabaAliqinFcSmsNumSendRequest;
            $req->setSmsType("normal");
            $req->setSmsFreeSignName("变更验证");   //短信签名固定，不可以换其他字
            $req->setSmsParam('{"code":"'. $smscode .'","product":"【多多助店宝】"}');
            $req->setRecNum("{$tel}");
            $req->setSmsTemplateCode("SMS_5056861");
            $resp = $c->execute($req); 
            if(!$resp->code) {
                //设置发送限制时间
                session($this->smstime, time() + 50);
                //设置验证码5分钟内有效
                session($this->smscode, array('code' => $smscode, 'time' => time() + 600));
            } else {
                //发送失败写入日志文件
                $log = date('Y-m-d H:i:s') . " 发送失败  sub_code：{$resp->sub_code}  sub_msg：{$resp->sub_msg}" . PHP_EOL;
                file_put_contents(RUNTIME_PATH . 'Log/smscode.log', $log, FILE_APPEND);
            }            
            $this->ajaxReturn(array('result' => !$resp->code));
        } else {
            $this->ajaxReturn(array('result' => false, 'error' => '错误的请求'));
        }
    }

    ?>