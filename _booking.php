<?php
require_once '_setup.php';
use Respect\Validation\Validator as Validator;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;

use PayPal\Api\Transaction;

$app->get('/booking/step1', function ($request, $response, $args) {            
    $locationList = DB::query("SELECT * from locations");
    $_SESSION['locationList'] = $locationList;
    $categoryList = DB::query("SELECT * from categories");
    $_SESSION['categoryList'] = $categoryList;
    return $this->view->render($response, 'booking/step1.html.twig', ['user' => $_SESSION['user'] ?? "", 'locationList' => $locationList]);    
});

$app->get('/booking/selectViewCategory/{categoryId:[0-9]+}/{sorterId:[0-9]+}', function ($request, $response, $args) { 
    $categoryId = $args['categoryId'] ?? 0;
    $sorterId = $args['sorterId'] ?? 0;
    switch($sorterId) {
        case "0": 
            $sortBy = "costPerDay";
            break;
        case "1":
            $sortBy = "costPerDay DESC";
            break;
        case "2":
            $sortBy = "passengersNo";
            break;
        case "3":
            $sortBy = "passengersNo DESC";
            break;
        default:
            break;
    }

    if($categoryId == "0") {
        $categoryList = DB::query("SELECT * from categories ORDER BY " . $sortBy);
    }
    else {
        $categoryList = DB::query("SELECT * from categories WHERE id = %d  ORDER BY " . $sortBy, $categoryId);
    }
    return $this->view->render($response, 'booking/selectViewCategory.html.twig', ['user' => $_SESSION['user'] ?? "",'categoryList' => $categoryList, 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});


$app->get('/booking/step2', function ($request, $response, $args) { 
    $categoryList = DB::query("SELECT * from categories");
    $_SESSION['categoryList'] = $categoryList;
    return $this->view->render($response, 'booking/step2.html.twig', ['user' => $_SESSION['user'] ?? "",'categoryList' => $_SESSION['categoryList'], 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});

$app->post('/booking/step2', function ($request, $response, $args) { 
    $errorList = array();

    $pickupDate = $request->getParam('pickupDate');
    $pickupTime = $request->getParam('pickupTime');
    $returnDate = $request->getParam('returnDate');
    $returnTime = $request->getParam('returnTime');
    //print_r($pickupTime);
    //print_r($returnTime);

    $booking['pickupDate'] = $pickupDate;
    $booking['pickupTime'] = $pickupTime;
    $booking['returnDate'] = $returnDate;
    $booking['returnTime'] = $returnTime;
    
    $pickupLocationId = $request->getParam('pickupLocation');
    $returnLocationId = $request->getParam('returnLocation');
    $pickupLocation = DB::queryFirstRow("SELECT id, street, city FROM locations WHERE id = %d", $pickupLocationId);
    $returnLocation = DB::queryFirstRow("SELECT id, street, city FROM locations WHERE id = %d", $returnLocationId);
    $booking['pickupLocation'] = $pickupLocation;
    $booking['returnLocation'] = $returnLocation;

/*    $pickupLocation = explode(",", $request->getParam('pickupLocation'));
    $booking['pickupStreet'] = $pickupLocation[0];
    $booking['pickupCity'] = $pickupLocation[1];
    $returnLocation =  explode(",", $request->getParam('returnLocation'));
    $booking['returnStreet'] = $returnLocation[0];
    $booking['returnCity'] = $returnLocation[1];
*/    /*
    $carList = DB::query("SELECT c.*, cat.categoryName FROM cars AS c JOIN categories AS cat ON c.categoryId = cat.id 
        WHERE c.id IN (SELECT carId FROM reservations WHERE startDateTime > %s OR returnDateTime < %s)", $strReturnDateTime, $strPickupDateTime);
    */
    $strPickupDateTime = $pickupDate . " " . $pickupTime;
    $strReturnDateTime = $returnDate . " " . $returnTime;

    $pickupDateTime = new DateTime($strPickupDateTime);
    $returnDateTime = new DateTime($strReturnDateTime);
    $now = date("Y-m-d H:i:s");
    $currentDateTime = new DateTime($now);
    $currentDateTime->modify('-6 hours');

    if($pickupDateTime < $currentDateTime) {
        $errorList[] = "Pick up Date/Time must be later than current Date/Time.";
    }
    if($pickupDateTime >= $returnDateTime) {
        $errorList[] = "Return Date/Time must be later than pick up Date/Time.";
    }

    $duration = $returnDateTime->diff($pickupDateTime);
    if ($duration->i >= 30) {
        $duration->h ++;
    }
    $duration->i = 0;
    if ($duration->h >= 10) {
        $duration->d ++;
        $duration->h = 0;
    }
    $strDuration = $duration->d == 0 ? "" : ($duration->d . ($duration->d > 1 ? " days " : " day "));
    $strDuration = $strDuration . ($duration->h == 0 ? "" : ($duration->h . ($duration->h > 1 ? " hours" : " hour")));
       
    $booking['strDuration'] = $strDuration;
    $booking['duration'] = $duration; 
    $booking['pickupDateTime'] = $pickupDateTime;
    $booking['returnDateTime'] = $returnDateTime;

    $_SESSION['booking'] = $booking;
    
    $categoryList = DB::query("SELECT * from categories");
    $_SESSION['categoryList'] = $categoryList;

    if($errorList) {
        $locationList = DB::query("SELECT * from locations");
        
        print_r($categoryList);
        return $this->view->render($response, 'booking/step1.html.twig', ['errorList' => $errorList, 'user' => $_SESSION['user'] ?? "", 'categoryList' => $categoryList, 'booking' => $_SESSION['booking'], 'locationList' => $locationList]);
    }

    return $this->view->render($response, 'booking/step2.html.twig', ['user' => $_SESSION['user'] ?? "", 'categoryList' => $_SESSION['categoryList'], 'booking' => $_SESSION['booking'], 'stepNo' => 2]);
});

$app->post('/booking/step3/{categoryId}', function ($request, $response, $args) {     
    $categoryId = $args['categoryId'];    
    $category = DB::queryFirstRow("SELECT * FROM categories WHERE id = %d", $categoryId);
    $_SESSION['category'] = $category;
    return $this->view->render($response, 'booking/step3.html.twig', ['user' => $_SESSION['user'] ?? "", 'booking' => $_SESSION['booking'], 'category' => $_SESSION['category'], 'stepNo' => 3]);
});

$app->post('/booking/step4', function ($request, $response, $args) {     
    if (isset($_SESSION['user'])) {
        $customer = DB::queryFirstRow("SELECT * FROM customers WHERE userId = %d", $_SESSION['user']['id']);
    }
    if (isset($_POST['confirmBooking'])) {
        //return $this->view->render($response, 'booking/step4.html.twig', ['booking' => $_SESSION['booking'], 'customer' => $customer, 'category' => $_SESSION['category'], 'stepNo' => 4]);
    }
    else {
        return $this->view->render($response, 'booking/step4.html.twig', ['user' => $_SESSION['user'] ?? "", 'booking' => $_SESSION['booking'], 'customer' => $customer, 'category' => $_SESSION['category'], 'stepNo' => 4]);
    }

/*    $booking = $_SESSION['booking'];
    $days = $booking['duration']->format('%a');
    $hours = $booking['duration']->format('%h');
    $pricePerDay = $_SESSION['category']['costPerDay'];
    $TotalPrice = $pricePerDay * $days + $pricePerDay * $hours * 0.1;
    $deposit = $TotalPrice * 0.1;
    print_r($TotalPrice);
    print_r($deposit);
*/
});

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/booking/payment/redirect")) {
        if (!recaptcha()) {
            $response = $response->withStatus(403);
            return $this->view->render($response, 'booking/error_access_denied.html.twig');
        }
        return $next($request, $response);
    }
    return $next($request, $response);
});

$app->post('/booking/payment/redirect', function ($request, $response, $args) {
    $firstName = $request->getParam('firstName');
    $lastName = $request->getParam('lastName');
    $phoneNo = $request->getParam('phoneNo');
    $street = $request->getParam('street');
    $city = $request->getParam('city');
    $province = $request->getParam('province');
    $postalCode = $request->getParam('postalCode');
    $paymentMethod = $request->getParam('paymentMethod');

    //save customer infomation to Database
    $customer['firstName'] = $firstName;
    $customer['lastName'] = $lastName;
    $customer['phoneNo'] = $phoneNo;
    $customer['street'] = $street;
    $customer['city'] = $city;
    $customer['province'] = $province;
    $customer['postalCode'] = $postalCode;

    $errorList = array();
    if (!(validator::stringType()->length(1, 50)->validate($customer['firstName']))) {
        $errorList[] = "First name must be 1-50 characters long, ";
    }
    if (!(validator::stringType()->length(1, 50)->validate($customer['lastName']))) {
        $errorList[] = "Last name must be 1-50 characters long, ";
    }
    if (!(validator::numericVal()->length(10, 15)->validate($customer['phoneNo']))) {
        $errorList[] = "Phone name must be 10-15 digital numbers long, ";
    }
    if (!(validator::length(0, 100)->validate($customer['street']))) {
        $errorList[] = "Street name must be 0-100 characters long, ";
    }
    if (!(validator::length(0, 50)->validate($customer['city']))) {
        $errorList[] = "City name must be 0-30 characters long, ";
    }
    if (!(validator::length(0, 30)->validate($customer['province']))) {
        $errorList[] = "Province name must be 0-30 characters long, ";
    }
    if (!(validator::length(0, 10)->validate($customer['postalCode']))) {
        $errorList[] = "Postal code must be 0-10 characters long, ";
    }
    if ($errorList) {
        return $this->view->render($response, 'booking/step4.html.twig', ['errorList' => $errorList, 'user' => $_SESSION['user'] ?? "", 'booking' => $_SESSION['booking'], 'customer' => $customer, 'category' => $_SESSION['category'], 'stepNo' => 4]);
    }

    $existCustomer = DB::queryFirstRow("SELECT id, userId FROM customers WHERE userId = %d", $_SESSION['user']['id']);
    if ( !$existCustomer) {
        $customer['userId'] = $_SESSION['user']['id'];
        DB::insert("customers", $customer);
        $customer['id'] = DB::insertId();
    }
    else {
        DB::update("customers", $customer, "userId = %d", $_SESSION['user']['id']);
        $customer['id'] = $existCustomer['id'];
    }
    $_SESSION['customerId'] = $customer['id'];

    //insert reservation information to Database
    $reservation['startDateTime'] = $_SESSION['booking']['pickupDateTime'];
    $reservation['returnDateTime'] = $_SESSION['booking']['returnDateTime'];
    $reservation['pickupLocationId'] = $_SESSION['booking']['pickupLocation']['id'];
    $reservation['returnLocationId'] = $_SESSION['booking']['returnLocation']['id'];
    $reservation['costPerDay'] = $_SESSION['category']['costPerDay'];
    $reservation['customerId'] = $_SESSION['customerId'];
    $invoiceNumber = uniqid();
    $reservation['invoiceNo'] = $invoiceNumber;
    $reservation['categoryId'] = $_SESSION['category']['id'];

    DB::insert('reservations', $reservation);

    if ($paymentMethod == 'Paypal') {
        $deposit = $request->getParam('deposit');
        $_SESSION['deposit']= $deposit;
        
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $amount = new Amount();
        $amount->setCurrency("CAD")
            ->setTotal($deposit);
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("Booking Car")
            ->setInvoiceNumber($invoiceNumber);
        $_SESSION['invoiceNo'] = $invoiceNumber;
       
        $redirectUrls = new RedirectUrls();    
        
        $redirectUrls->setReturnUrl("http://carrental.ipd21.com/booking/payment/exec?success=true")
                     ->setCancelUrl("http://carrental.ipd21.com/booking/payment/cancel?success=false");

        //$redirectUrls->setReturnUrl("http://project-carrental.ipd21:8888/booking/payment/exec?success=true")
        //->setCancelUrl("http://roject-carrental.ipd21:8888/booking/payment/cancel?success=false");
        
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));
        $payment->create(getApiContext());

        $approvalUrl = $payment->getApprovalLink();
        $response = $response->withStatus(302);
        return $response->withHeader('Location', $approvalUrl);
        
    }
    else {
        return $this->view->render($response, 'booking/operation_success.html.twig', [ 'message' => 'Your booking reserved success.']);
    }
});
//return $response->withHeader('Location', '/booking/payment/exec');

$app->get('/booking/payment/exec', function ($request, $response, $args) {    
    if (empty($_GET['paymentId']) || empty($_GET['PayerID'])) {
        throw new Exception('The response is missing the paymentId and PayerID');
    }
    
    $paymentId = $_GET['paymentId'];
    $payment = Payment::get($paymentId, getApiContext());
    
    $execution = new PaymentExecution();
    $execution->setPayerId($_GET['PayerID']);
    
    $payment->execute($execution, getApiContext());
   
    $invoiceNumber = $payment->transactions[0]->invoice_number;
    DB::update('reservations', [ 'isDepositPayed' => 1 ], "invoiceNo = %d", $invoiceNumber);

    return $this->view->render($response, 'booking/operation_success.html.twig', ['message' => "Car Rental Reserved Successfully."]);
    
 });

$app->get('/booking/payment/cancel', function ($request, $response, $args) {
    return $this->view->render($response, 'booking/payment_cancel.html.twig');

});

function getApiContext() {
    $clientId = 'AUdKBCHm9jjhpJ35UC_RfA6WJs0EMDTLvgY5UOjL2-hu6jqAoB_d5G5tMocbLgLkj_qFoBDEc2F4Jn-M';
    $clientSecret = 'ENiOG9-N7mZkSC4TUKUT4Wpsu6swW1KjXg-lSAWybYQfCMzeAIAUy_tM-dz56aEmud44e9AvDql8xOcV';
    $apiContext = new ApiContext(
        new OAuthTokenCredential(
            $clientId,
            $clientSecret
        )
    );
    $apiContext->setConfig(
        array(
            'mode' => 'sandbox',
            'log.LogEnabled' => true,
            'log.FileName' => './PayPal.log',
            'log.LogLevel' => 'DEBUG', 
            'cache.enabled' => true
        )
    );
    return $apiContext;
}

function recaptcha() {
    if(isset($_POST['g-recaptcha-response'])){
        $captcha=$_POST['g-recaptcha-response'];
    }
    if(!$captcha){
        echo '<h2>Please check the the captcha form.</h2>';
        return false;
        exit;
    }
        
    $secretKey = "6LfxzbcZAAAAAOeWVvkN0v7bNkMEQfDXNSEmUICl";
    $ip = $_SERVER['REMOTE_ADDR'];

    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
    $response = file_get_contents($url);
    $responseKeys = json_decode($response,true);

    if(!$responseKeys["success"]) {
        echo "Error";
        return false;
        exit;
    }
        return true;
}
    
    
/*
$app->post('/booking/payment', function ($request, $response, $args) {
    $clientId = 'AUdKBCHm9jjhpJ35UC_RfA6WJs0EMDTLvgY5UOjL2-hu6jqAoB_d5G5tMocbLgLkj_qFoBDEc2F4Jn-M';
        $clientSecret = 'ENiOG9-N7mZkSC4TUKUT4Wpsu6swW1KjXg-lSAWybYQfCMzeAIAUy_tM-dz56aEmud44e9AvDql8xOcV';
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );
        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName' => './PayPal.log',
                'log.LogLevel' => 'DEBUG', 
                'cache.enabled' => true
            )
        );

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
        $item1 = new Item();
        $item1->setName('test pro 1')
            ->setCurrency('CAD')
            ->setQuantity(1)
            ->setSku("testpro1_01") 
            ->setPrice(0.1);
        $itemList = new ItemList();
        $itemList->setItems(array($item1));
        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal(0.1);

        // 同上，金额要相等
        $amount = new Amount();
        $amount->setCurrency("CAD")
            ->setTotal(0.1)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();    
        $redirectUrls->setReturnUrl("http://project-carrental.ipd21:8888/payment/exec.php?success=true")
            ->setCancelUrl("http://project-carrental.ipd21:8888/payment/cancel.php?success=false");

        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));
        $payment->create($apiContext);
        //生成地址
        $approvalUrl = $payment->getApprovalLink();
    $response = $response->withStatus(302);
    //return $response->withHeader('Location', '/payment/payment.php');
    return $response->withHeader('Location', $approvalUrl);
});

*/

