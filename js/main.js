
/* ******************* Validation for add new admin start ******************* */
$("#add-admin").validate({
    errorElement: "em",
    rules: {
        vEmail: {
            required: true,
            email: true
        },
        vPassword: {
            required: true,
            minlength: 3
        },
        vConfPassword: {
            required: true,
            equalTo: "#vPassword"
        }

    },
    messages: {
        vEmail: {
            required: "Please, enter email id",
            email: "Please, enter valid email id"
        },
        vPassword: {
            required: "Please, enter password"
        },
        vConfPassword: {
            required: "Please, enter confirm password",
            equalTo: "Password and confirm password does not match"
        }

    }

});

/* ******************* Validation for add new admin end ******************* */

/* ******************* Validation for add news start ******************* */
$("#news_form").validate({
    rules: {
        vImage: {
                extension: "jpg,jpeg,png",
                //filesize: 1048576
                filesize:2097152 // It's 2 MB
         },
        vNews_desc: {
            required: true,
            minlength: 5
        }

    },
    messages: {
         vImage: {
            extension: "Only jpeg or png file support",
            filesize:"Your image must be less than 2 MB" // It's 2 MB
        },
        vNews_desc: {
            required: "Please, enter description"
        }

    }

});

/* ******************* Validation for add news end ******************* */

/* ******************* Validation for Gallary start ******************* */
$("#gallary-form").validate({
    rules: {
     'vImage[]': {
                
                extension: "jpg,jpeg,png",
                //filesize: 1048576
                filesize:2097152 // It's 2 MB
            }
    },
    messages: {
         'vImage[]': {
            extension: "Only jpeg or png file support",
            filesize:"Your image must be less than 2 MB" // It's 2 MB
        }

    }

});

/* ******************* Validation Gallary end ******************* */

/* ******************* Validation for Banner upload start ******************* */
$("#banner-form").validate({
    rules: {
     'vImage[]': {
                
                extension: "jpg,jpeg,png",
                //filesize: 1048576
                filesize:2097152 // It's 2 MB
            }
    },
    messages: {
         'vImage[]': {
            extension: "Only jpeg or png file support",
            filesize:"Your image must be less than 2 MB" // It's 2 MB
        }

    }

});

/* ******************* Validation Banner upload end ******************* */


/* ******************* Change password start ******************* */
$("#change-pwd").validate({
    errorElement: "em",
    rules: {
        new_pwd: {
            required: true,
            minlength: 3
        },
        conf_password: {
            required: true,
            equalTo: "#pwd"
        }

    },
    messages: {
        new_pwd: {
            required: "Please, enter password"
        },
        conf_password: {
            required: "Please, enter confirm password",
            equalTo: "Password and confirm password does not match"
        }

    }

});

/* ******************* Change password end ******************* */
