<?php
require_once '_setup.php';
// article add
$app->get('/articleadd', function ($request, $response, $args) {  
    if (!isset($_SESSION['user'])) {
        $response = $response -> withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }    
    return $this->view->render($response, 'articleadd.html.twig', ['user' => ($_SESSION['user'] ?? '')]);    
});

$app->post('/articleadd', function ($request, $response, $args) {
    $title = $request->getParam('title');
    $body = $request->getParam('body');    

    $errorList = array();

    if (strlen($title) < 10 || strlen($title) > 100) {
        $errorList[] = "Title must between 10-100 characters";
    }

    $body = strip_tags($body, "");

    if (strlen($body) < 50 || strlen($body) > 10000) {
        $errorList[] = "Content must between 50-10000 characters.";        
    }    
    
    if (isset($_FILES['photo']) && $_FILES['photo']['error']!= 4) {
        $photo = $_FILES['photo'];        

        if ($photo['error'] != 0) {
            $errorList[] = "Error uploading photo.";
        } 
        else {                        
            $info = getimagesize($photo['tmp_name']);
            if (!$info) {
                $errorList[] = "File is not a image.";
            } 
            else {                    
                $ext = "";
                switch ($info['mime']) {
                    case "image/jpeg": $ext="jpg"; break;
                    case "image/gif": $ext="gif"; break;
                    case "image/png": $ext="png"; break;
                    default: 
                        $errorList[] = "Only JPG, GIF and PNG file types are allowed";
                }
                if ($ext) {
                    $fileName = explode('.', $photo['name'])[0];
                    $photoFilePath = "../uploads/" . $fileName . "." . $ext;
                }                
            }                                       
        }
    }    

    if ($errorList) {
        return $this->view->render($response, 'articleadd.html.twig', ['errorList' => $errorList, 'user' => ($_SESSION['user'] ?? ''), 'title' => $title, 'body' => $body ]);
    }
    else {
        DB::insert("articles", ['title' => $title, 'body' => $body, 'authorId' => $_SESSION['user']['id'], 'photoFilePath' => $photoFilePath]);
        $articleId = DB::insertId();    
        if ($photoFilePath != NULL) {
            move_uploaded_file($photo['tmp_name'], $photoFilePath);  
        }  
        return $this->view->render($response, 'articleadd_success.html.twig', [ 'articleId' => $articleId ]);
    }
});

// edit article
$app->get('/editarticle/{articleId}', function ($request, $response, $args) {  
    if (!isset($_SESSION['user'])) {
        $response = $response -> withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }    
    return $this->view->render($response, 'editarticle.html.twig', ['user' => ($_SESSION['user'] ?? ''), 'article' => $_SESSION['article']]);    
});

$app->post('/editarticle/{articleId}', function ($request, $response, $args) {
    $title = $request->getParam('title');
    $body = $request->getParam('body');    
    $articleId = $args['articleId'];

    $errorList = array();

    if (strlen($title) < 10 || strlen($title) > 100) {
        $errorList[] = "Title must between 10-100 characters";
    }

    if (strlen($body) < 50 || strlen($body) > 10000) {
        $errorList[] = "Content must between 50-10000 characters.";        
    }    
    
    if (isset($_FILES['photo']) && $_FILES['photo']['error']!= 4) {
        $photo = $_FILES['photo'];        

        if ($photo['error'] != 0) {
            $errorList[] = "Error uploading photo.";
        } 
        else {                        
            $info = getimagesize($photo['tmp_name']);
            if (!$info) {
                $errorList[] = "File is not a image.";
            } 
            else {                    
                $ext = "";
                switch ($info['mime']) {
                    case "image/jpeg": $ext="jpg"; break;
                    case "image/gif": $ext="gif"; break;
                    case "image/png": $ext="png"; break;
                    default: 
                        $errorList[] = "Only JPG, GIF and PNG file types are allowed";
                }
                if ($ext) {
                    $fileName = explode('.', $photo['name'])[0];
                    $photoFilePath = "uploads/" . $fileName . "." . $ext;
                }                
            }                                       
        }
    }    

    if ($errorList) {
        return $this->view->render($response, 'editarticle.html.twig', ['errorList' => $errorList, 'user' => ($_SESSION['user'] ?? ''), 'title' => $title, 'body' => $body ]);
    }
    else {
        DB::update("articles", ['title' => $title, 'body' => $body, 'photoFilePath' => '../' . $photoFilePath], "id = %d", $articleId);       
        if ($photoFilePath != NULL) {
            move_uploaded_file($photo['tmp_name'], $photoFilePath);  
        }  
        return $this->view->render($response, 'editarticle_success.html.twig', [ 'articleId' => $articleId ]);
    }
});


// article
$app->get('/article/{articleId:[0-9]+}', function ($request, $response, $args) {
    $articleId = $args['articleId'];
    $article = DB::queryFirstRow("SELECT a.id, authorId, creationTS, title, body, photoFilePath, u.name FROM articles AS a JOIN users AS u WHERE a.authorId = u.id AND a.id = %d LIMIT 1", $articleId);
    if (!$article) {
        // TO DO
    }

    $datetime = strtotime($article['creationTS']);
    $postedDate = date('M d, Y \a\t H:i:s', $datetime );
    $article['postedDate'] = $postedDate;
    $_SESSION['article'] = $article;

    $comments = DB::query("SELECT * FROM comments WHERE articleId = %d", $article['id']);
    foreach($comments as &$comment) {
        $datetime = strtotime($comment['creationTS']);
        $postedDate = date('M d, Y \a\t H:i:s', $datetime );
        $comment['postedDate'] = $postedDate;
    }

    return $this->view->render($response, 'article.html.twig', ['article' => $article, 'comments' => $comments, 'user' => ($_SESSION['user'] ?? '')]);
});


//$app->map(['GET','POST'], '/article/{articleId}', function ($request, $response, $args) {
$app->post('/article/{articleId}', function ($request, $response, $args) {
    if (!isset($_SESSION['user'])) {
        $response = $response -> withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }    
    else 
    {
        $articleId = $args['articleId'];
        $body = $request->getParam('myComment');
        DB::insert("comments", ['articleId' => $articleId, 'authorId' => $_SESSION['user']['id'], 'body' => $body]);
        return $this->view->render($response, 'addComment_success.html.twig', ['articleId' => $articleId]);
    }

});


// use ajax
$app->get('/ajax/articlecommentadd/{articleId}',function ($request, $response, $args) {
    //return $this->view->render($response, 'article.html.twig', ['article' => $article, 'comments' => $comments, 'user' => ($_SESSION['user'] ?? '')]);
});


$app->post('/ajax/articlecommentadd/{articleId}',function ($request, $response, $args) {
    print_r($_POST['commentForm']);

/*    if (!isset($_SESSION['user'])) {
        $response = $response -> withStatus(403);
        return $this->view->render($response, 'error_access_denied.html.twig');
    }    
    else 
    {
        $articleId = $args['articleId'];
        $body = $request->getParam('myComment');
        DB::insert("comments", ['articleId' => $articleId, 'authorId' => $_SESSION['user']['id'], 'body' => $body]);
        return $this->view->render($response, 'addComment_success.html.twig', ['articleId' => $articleId]);
    }
*/
});
