<?php

require_once '_setup.php';
require_once '_main.php';
require_once '_user.php';

require_once '_admin.php';
require_once '_adminUser.php';
require_once '_car.php';
require_once '_customer.php';
require_once '_reservation.php';
require_once '_location.php';
require_once '_category.php';
require_once '_booking.php';

// Run app
$app->run();


